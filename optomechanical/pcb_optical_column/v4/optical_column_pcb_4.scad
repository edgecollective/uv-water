$fn = 100;
WALL_THICKNESS = 2.5;

// design for M3 brass heat set inserts
THREAD_INSERT_HOLE_DIAM = 4.75;
THREAD_INSERT_HOLE_LENGTH = 6;
THREAD_INSERT_HOLE_OFFSET = 30;

HEADER_HEIGHT = 8.5;

PCB_WIDTH  = 34;
PCB_HEIGHT = PCB_WIDTH; 
PCB_STAND_OFF_DEPTH = 2;
PCB_STAND_OFF_DIAM  = THREAD_INSERT_HOLE_DIAM + 4;
PCB_THICKNESS = 1.6;
PCB_RELIEF   = PCB_STAND_OFF_DEPTH + PCB_THICKNESS;
PCB_GAP = 1;
PCB_MOUNT_SPACING = 26;

OPTICS_SHROUD_WALL_T = 1;
OPTICS_SHROUD_ID = 8;
//OPTICS_SHROUD_OD = OPTICS_SHROUD_ID+2*OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_OD = 12;
OPTICS_SHROUD_BASE_D = OPTICS_SHROUD_OD + OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_BASE_HEIGHT = 1.5*PCB_STAND_OFF_DEPTH;
OPTICS_SHROUD_HEIGHT = 3 + OPTICS_SHROUD_BASE_HEIGHT;

CUVETTE_HEIGHT = 45;
CUVETTE_WIDTH  = 12.6;
CUVETTE_DEPTH  = 12.4;
CUVETTE_GAP    = 0.5;
CUVETTE_HEIGHT_ABOVE_TOP = 10;

CUVETTE_CAVITY_WIDTH = CUVETTE_WIDTH+2*CUVETTE_GAP;
CUVETTE_CAVITY_DEPTH = CUVETTE_DEPTH+2*CUVETTE_GAP;


COLUMN_WIDTH = PCB_WIDTH + 4*WALL_THICKNESS;
COLUMN_DEPTH = CUVETTE_DEPTH + 6*WALL_THICKNESS;
COLUMN_HEIGHT = PCB_HEIGHT + 2*PCB_RELIEF + 4*WALL_THICKNESS;

COLUMN_MINK_R = 2;



// plug of flexible material for holding cuvette
GRIP_PLUG_WIDTH  = CUVETTE_CAVITY_WIDTH + 2*WALL_THICKNESS;
GRIP_PLUG_DEPTH  = CUVETTE_CAVITY_DEPTH + 2*WALL_THICKNESS;
GRIP_PLUG_HEIGHT = CUVETTE_HEIGHT_ABOVE_TOP + 2*WALL_THICKNESS;
GRIP_PLUG_GAP = 0.1;
GRIP_INNER_KNURL_R = 1.1*CUVETTE_GAP;//
GRIP_INNER_KNURL_NUM = 4;

// socket in top of column to fit grip plug
GRIP_SOCKET_WIDTH = GRIP_PLUG_WIDTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_DEPTH = GRIP_PLUG_DEPTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_HEIGHT = GRIP_PLUG_HEIGHT;

// base plug, fits into bottom, stops cuvete moving down, dampens vibration
// should be printed in flexible, soft, material
BASE_WIDTH = COLUMN_WIDTH + 4*WALL_THICKNESS;
BASE_DEPTH = COLUMN_DEPTH + 4*WALL_THICKNESS;
BASE_HEIGHT = 2*WALL_THICKNESS;
BASE_FOOT_DIAM = 8;
BASE_FOOT_HEIGHT = WALL_THICKNESS;


BASE_PLUG_WIDTH = CUVETTE_CAVITY_WIDTH;
BASE_PLUG_DEPTH = CUVETTE_CAVITY_DEPTH;
BASE_PLUG_HEIGHT = COLUMN_HEIGHT - (CUVETTE_HEIGHT - CUVETTE_HEIGHT_ABOVE_TOP) + BASE_HEIGHT;
BASE_PLUG_MINK_R = 2;

M3_THRU_HOLE_DIAM = 3.4;
M3_HEAD_CLEAR_DIAM = 6;
M3_NUT_THICKNESS=3;
M3_BODY_THICKNESS=5;

DETECTOR_HEIGHT=6;


//PCB_PCB = COLUMN_DEPTH-(PCB_STAND_OFF_DEPTH);
PCB_PCB = COLUMN_DEPTH - PCB_THICKNESS*2;
//PCB_PCB = 28;
translate([-COLUMN_WIDTH/2,-PCB_PCB/2,0])
color("blue",0.6)
//cube([COLUMN_WIDTH,PCB_PCB,COLUMN_HEIGHT]);

echo(PCB_PCB=PCB_PCB);

PCB_PCB_OUTER = 28+PCB_THICKNESS;
//PCB_PCB_OUTER = 20;


//pcb


module pcb(){
   difference(){
   translate([-PCB_WIDTH/2,0,0]) 
    color("green",.6)
cube([PCB_WIDTH,PCB_THICKNESS,PCB_HEIGHT]);
       
       //bottom left mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2]) 
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

       
       //bottom right mount
translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);
   
       //top left mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);   

//top right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);
}
       
}


PLATE_MOUNT_SPACING=PCB_WIDTH+2.5*M3_THRU_HOLE_DIAM;

SLIT_WIDTH=PCB_THICKNESS*1.1;
SLIT_HEIGHT=12;

PLATE_WIDTH=PLATE_MOUNT_SPACING+10*SLIT_WIDTH;
PLATE_HEIGHT=PCB_WIDTH+M3_THRU_HOLE_DIAM*2;
//emitter_side_plate

//STANDOFF_Z_INSET=2*M3_THRU_HOLE_DIAM;
STANDOFF_Z_INSET=(PCB_WIDTH-PCB_MOUNT_SPACING)/2+M3_THRU_HOLE_DIAM*.9;
STANDOFF_X_OFFSET=M3_HEAD_CLEAR_DIAM*1.3;

LEFT_WALL_X_OFFSET=PCB_THICKNESS;

WALL_LENGTH=PCB_PCB_OUTER-PCB_THICKNESS*2;

WALL_TAB_WIDTH=6;
WALL_TAB_LENGTH=2;

module wall(factor) {
translate([0,-WALL_LENGTH/2,0])
color("blue",1)
cube([PCB_THICKNESS*factor,WALL_LENGTH,PLATE_HEIGHT]);

//tab
translate([0,-WALL_LENGTH/2-WALL_TAB_LENGTH,PLATE_HEIGHT/2-WALL_TAB_WIDTH])
color("blue",1)
cube([PCB_THICKNESS*factor,WALL_TAB_LENGTH*factor,WALL_TAB_WIDTH*factor*2]);

//other tab

translate([0,WALL_LENGTH/2,PLATE_HEIGHT/2-WALL_TAB_WIDTH])
color("blue",1)
cube([PCB_THICKNESS*factor,WALL_TAB_LENGTH*factor,WALL_TAB_WIDTH*factor*2]);
    
}

    PCB_VERTICAL_SHIFT=3;

TAB_LENGTH=2;
TAB_WIDTH=8;

FLOOR_LEVEL=PCB_THICKNESS;
MID_HOLDER_LEVEL=STANDOFF_Z_INSET*1.5;
//MID_HOLDER_LEVEL=STANDOFF_Z_INSET+(STANDOFF_Z_INSET-PCB_THICKNESS);
//MID_HOLDER_LEVEL=STANDOFF_Z_INSET-FLOOR_LEVEL;

UPPER_HOLDER_LEVEL=PLATE_HEIGHT-M3_THRU_HOLE_DIAM;
NEAR_TOP_LEVEL=PCB_HEIGHT-STANDOFF_Z_INSET;


module floor(factor) {
translate([-PLATE_WIDTH/2+TAB_LENGTH,-PCB_PCB_OUTER/2+PCB_THICKNESS,0])
color("purple",1)
cube([PLATE_WIDTH-2*TAB_LENGTH,PCB_PCB_OUTER-2*PCB_THICKNESS,PCB_THICKNESS*factor]);

//tab
translate([-PLATE_WIDTH/2,-TAB_WIDTH/2,0])
color("purple",1)
cube([TAB_LENGTH*factor,TAB_WIDTH*factor,PCB_THICKNESS*factor]);  

//other tab  
  translate([PLATE_WIDTH/2-TAB_LENGTH,-TAB_WIDTH/2,0])
color("purple",1)
cube([TAB_LENGTH*factor,TAB_WIDTH*factor,PCB_THICKNESS*factor]);  
}


module holder(factor) {
difference(){
floor(factor);
    translate([-CUVETTE_CAVITY_WIDTH/2,-CUVETTE_CAVITY_WIDTH/2,-.1])
color("blue",0.6)
cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_WIDTH,PCB_THICKNESS*1.1]);
}
}




module plate(){
difference(){
translate([-PLATE_WIDTH/2,0,0])
color("purple",1)
cube([PLATE_WIDTH,PCB_THICKNESS,PLATE_HEIGHT]);


// pcb_hole
    translate([0,PCB_THICKNESS/2,PCB_HEIGHT/2+PCB_VERTICAL_SHIFT])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=OPTICS_SHROUD_OD,center=true);
 
  
    
//bottom left plate mount
     translate([-PLATE_WIDTH/2+STANDOFF_X_OFFSET,PCB_THICKNESS/2,STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right plate mount
    translate([PLATE_WIDTH/2-STANDOFF_X_OFFSET,PCB_THICKNESS/2,STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);

//top left plate mount
    translate([-PLATE_WIDTH/2+STANDOFF_X_OFFSET,PCB_THICKNESS/2,PLATE_HEIGHT-STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);  

//top right plate mount
    translate([PLATE_WIDTH/2-STANDOFF_X_OFFSET,PCB_THICKNESS/2,PLATE_HEIGHT-STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);

translate([0,0,PCB_VERTICAL_SHIFT]){
//bottom left pcb mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);

//top left pcb mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);   

//top right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.01,d=M3_THRU_HOLE_DIAM,center=true);
}
/*
//left slot
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,SLIT_WIDTH/2,PLATE_HEIGHT/2])
color("green",0.6)
//rotate([90,0,0])
cylinder(h=SLIT_HEIGHT,d=SLIT_WIDTH*1.1,center=true);

//right slot 
translate([PLATE_WIDTH/2-LEFT_WALL_X_OFFSET,SLIT_WIDTH/2,PLATE_HEIGHT/2])
color("green",0.6)
//rotate([90,0,0])
cylinder(h=SLIT_HEIGHT,d=SLIT_WIDTH*1.1,center=true);
*/

}
}

module detector() {    
    color("blue",1)
rotate([90,0,0])
cylinder(h=DETECTOR_HEIGHT*.8,d=DETECTOR_HEIGHT,center=true);
}

module standoff() {
        //translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("yellow",1)
rotate([90,0,0])
//cylinder(h=PCB_PCB_OUTER+2*PCB_THICKNESS,d=M3_THRU_HOLE_DIAM,center=true);
cylinder(h=PCB_PCB_OUTER+2*PCB_THICKNESS,d=M3_BODY_THICKNESS,center=true);

}

module nut() {

rotate([90,0,0])
    //translate([0,0,(PCB_PCB_OUTER+2*PCB_THICKNESS)/2+M3_NUT_THICKNESS/2])
    cylinder(h=M3_NUT_THICKNESS,d=M3_HEAD_CLEAR_DIAM,center=true);
    //rotate([90,0,0])
    //translate([0,0,-(PCB_PCB_OUTER+2*PCB_THICKNESS)/2-M3_NUT_THICKNESS/2])
    //cylinder(h=M3_NUT_THICKNESS,d=M3_NUT_THICKNESS*2,center=true);    
    
}




// 0: default
// 4: plate projection
// 5: wall projection
// 6: floor projection
// 7: holder projection

VIEW=0 ; 


if(VIEW==0) {
    
    FACTOR=1.;
    HOLDER_FACTOR=1;
    
    
// 'FLOORS'
translate([0,0,FLOOR_LEVEL])
floor(HOLDER_FACTOR);

translate([0,0,MID_HOLDER_LEVEL])
holder(HOLDER_FACTOR);

translate([0,0,NEAR_TOP_LEVEL])
holder(HOLDER_FACTOR);

translate([0,0,UPPER_HOLDER_LEVEL])
holder(HOLDER_FACTOR);


// 'CUVETTE'
translate([-CUVETTE_WIDTH/2,-CUVETTE_WIDTH/2,FLOOR_LEVEL+PCB_THICKNESS])
color("LightBlue",0.3)
cube([CUVETTE_WIDTH,CUVETTE_WIDTH,CUVETTE_HEIGHT]);



// 'WALLS'
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,0,0])
wall(FACTOR);

translate([PLATE_WIDTH/2-2*LEFT_WALL_X_OFFSET,0,0])
wall(FACTOR);




// 'STANDOFFS and NUTS
//bottom left standoff
translate([-PLATE_WIDTH/2+STANDOFF_X_OFFSET,0,STANDOFF_Z_INSET]) {
    translate([0,(PCB_PCB_OUTER+2*PCB_THICKNESS)/2+M3_NUT_THICKNESS/2,0])
    nut();
     translate([0,-(PCB_PCB_OUTER+2*PCB_THICKNESS)/2-M3_NUT_THICKNESS/2,0])
    nut();
standoff();
}
//bottom right standoff
    translate([PLATE_WIDTH/2-STANDOFF_X_OFFSET,0,STANDOFF_Z_INSET]) {
standoff();
         translate([0,(PCB_PCB_OUTER+2*PCB_THICKNESS)/2+M3_NUT_THICKNESS/2,0])
    nut();
     translate([0,-(PCB_PCB_OUTER+2*PCB_THICKNESS)/2-M3_NUT_THICKNESS/2,0])
    nut();
    }

//top left standoff
    //translate([-STANDOFF_X_OFFSET,0,PCB_HEIGHT-STANDOFF_Z_INSET])
    translate([-PLATE_WIDTH/2+STANDOFF_X_OFFSET,0,PLATE_HEIGHT-STANDOFF_Z_INSET]) {
standoff();  
        translate([0,(PCB_PCB_OUTER+2*PCB_THICKNESS)/2+M3_NUT_THICKNESS/2,0])
    nut();
     translate([0,-(PCB_PCB_OUTER+2*PCB_THICKNESS)/2-M3_NUT_THICKNESS/2,0])
    nut();
    }
    
//top right standoff
    translate([PLATE_WIDTH/2-STANDOFF_X_OFFSET,0,PLATE_HEIGHT-STANDOFF_Z_INSET]) {
standoff();
        translate([0,(PCB_PCB_OUTER+2*PCB_THICKNESS)/2+M3_NUT_THICKNESS/2,0])
    nut();
     translate([0,-(PCB_PCB_OUTER+2*PCB_THICKNESS)/2-M3_NUT_THICKNESS/2,0])
    nut();
    }




// 'PLATES'
translate([0,PCB_PCB_OUTER/2-PCB_THICKNESS,0])
plate();

translate([0,-PCB_PCB_OUTER/2,0])
plate();



// 'PCBS'
translate([0,-PCB_PCB_OUTER/2-PCB_THICKNESS,PCB_VERTICAL_SHIFT])
pcb();
    
// detector
//translate([0,-PCB_PCB_OUTER/2+PCB_THICKNESS,PCB_HEIGHT/2])
//detector();
    
translate([0,PCB_PCB_OUTER/2,PCB_VERTICAL_SHIFT])
pcb();

}


if(VIEW==4) {

FACTOR=1.05;

  projection()
    rotate([90,0,0])
    translate([0,0,10])
difference() {
// 'PLATES'
//translate([0,PCB_PCB_OUTER/2-PCB_THICKNESS,0])
//plate();

    
translate([0,-PCB_PCB_OUTER/2,0])
plate();

// 'WALLS'
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,0,0])
wall(FACTOR);

translate([PLATE_WIDTH/2-2*LEFT_WALL_X_OFFSET,0,0])
wall(FACTOR);


}
}

if(VIEW==5) {
     FACTOR=1;
    
    HOLDER_FACTOR=1.05;
projection()
    rotate([0,90,0])
    translate([0,0,10])
difference() {
// 'WALLS'
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,0,0])
wall(FACTOR);



// 'FLOORS'
translate([0,0,FLOOR_LEVEL])
floor(HOLDER_FACTOR);

translate([0,0,MID_HOLDER_LEVEL])
floor(HOLDER_FACTOR);

translate([0,0,NEAR_TOP_LEVEL])
floor(HOLDER_FACTOR);

translate([0,0,UPPER_HOLDER_LEVEL])
floor(HOLDER_FACTOR);


}
    
    
    
}


if(VIEW==6) {
     FACTOR=1;
    
    HOLDER_FACTOR=1;
projection()
    //rotate([0,90,0])
    translate([0,0,10])

floor(HOLDER_FACTOR);

}


if(VIEW==7) {
     FACTOR=1;
    
    HOLDER_FACTOR=1;
projection()
    //rotate([0,90,0])
    translate([0,0,10])

holder(HOLDER_FACTOR);


    
}

