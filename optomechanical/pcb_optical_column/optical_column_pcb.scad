$fn = 100;
WALL_THICKNESS = 2.5;

// design for M3 brass heat set inserts
THREAD_INSERT_HOLE_DIAM = 4.75;
THREAD_INSERT_HOLE_LENGTH = 6;
THREAD_INSERT_HOLE_OFFSET = 30;

HEADER_HEIGHT = 8.5;

PCB_WIDTH  = 34;
PCB_HEIGHT = PCB_WIDTH; 
PCB_STAND_OFF_DEPTH = 2;
PCB_STAND_OFF_DIAM  = THREAD_INSERT_HOLE_DIAM + 4;
PCB_THICKNESS = 1.6;
PCB_RELIEF   = PCB_STAND_OFF_DEPTH + PCB_THICKNESS;
PCB_GAP = 1;
PCB_MOUNT_SPACING = 26;

OPTICS_SHROUD_WALL_T = 1;
OPTICS_SHROUD_ID = 8;
OPTICS_SHROUD_OD = OPTICS_SHROUD_ID + 2*OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_BASE_D = OPTICS_SHROUD_OD + OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_BASE_HEIGHT = 1.5*PCB_STAND_OFF_DEPTH;
OPTICS_SHROUD_HEIGHT = 3 + OPTICS_SHROUD_BASE_HEIGHT;

CUVETTE_HEIGHT = 45;
CUVETTE_WIDTH  = 12.6;
CUVETTE_DEPTH  = 12.4;
CUVETTE_GAP    = 0.5;
CUVETTE_HEIGHT_ABOVE_TOP = 10;

CUVETTE_CAVITY_WIDTH = CUVETTE_WIDTH+2*CUVETTE_GAP;
CUVETTE_CAVITY_DEPTH = CUVETTE_DEPTH+2*CUVETTE_GAP;


COLUMN_WIDTH = PCB_WIDTH + 4*WALL_THICKNESS;
COLUMN_DEPTH = CUVETTE_DEPTH + 6*WALL_THICKNESS;
COLUMN_HEIGHT = PCB_HEIGHT + 2*PCB_RELIEF + 4*WALL_THICKNESS;

COLUMN_MINK_R = 2;



// plug of flexible material for holding cuvette
GRIP_PLUG_WIDTH  = CUVETTE_CAVITY_WIDTH + 2*WALL_THICKNESS;
GRIP_PLUG_DEPTH  = CUVETTE_CAVITY_DEPTH + 2*WALL_THICKNESS;
GRIP_PLUG_HEIGHT = CUVETTE_HEIGHT_ABOVE_TOP + 2*WALL_THICKNESS;
GRIP_PLUG_GAP = 0.1;
GRIP_INNER_KNURL_R = 1.1*CUVETTE_GAP;//
GRIP_INNER_KNURL_NUM = 4;

// socket in top of column to fit grip plug
GRIP_SOCKET_WIDTH = GRIP_PLUG_WIDTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_DEPTH = GRIP_PLUG_DEPTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_HEIGHT = GRIP_PLUG_HEIGHT;

// base plug, fits into bottom, stops cuvete moving down, dampens vibration
// should be printed in flexible, soft, material
BASE_WIDTH = COLUMN_WIDTH + 4*WALL_THICKNESS;
BASE_DEPTH = COLUMN_DEPTH + 4*WALL_THICKNESS;
BASE_HEIGHT = 2*WALL_THICKNESS;
BASE_FOOT_DIAM = 8;
BASE_FOOT_HEIGHT = WALL_THICKNESS;


BASE_PLUG_WIDTH = CUVETTE_CAVITY_WIDTH;
BASE_PLUG_DEPTH = CUVETTE_CAVITY_DEPTH;
BASE_PLUG_HEIGHT = COLUMN_HEIGHT - (CUVETTE_HEIGHT - CUVETTE_HEIGHT_ABOVE_TOP) + BASE_HEIGHT;
BASE_PLUG_MINK_R = 2;

M3_THRU_HOLE_DIAM = 3.4;
M3_HEAD_CLEAR_DIAM = 6;

// should be printed with rigid material, preferably matte black
module column(){
    difference(){
        union(){
             difference(){
                 minkowski(){
                    cube([COLUMN_WIDTH-2*COLUMN_MINK_R,COLUMN_DEPTH-2*COLUMN_MINK_R,COLUMN_HEIGHT-2*COLUMN_MINK_R],center=true);
                    sphere(COLUMN_MINK_R);
                 }
                    // create cavity for cuvette
                    cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_DEPTH,2*CUVETTE_HEIGHT],center=true);
                    // make relief for source board
                    translate([0,-COLUMN_DEPTH/2 + PCB_RELIEF/2 - 0.01,0])
                    cube([PCB_WIDTH+2*PCB_GAP,PCB_RELIEF,PCB_HEIGHT+2*PCB_GAP],center=true);
                    // make relief for source board
                    translate([0, COLUMN_DEPTH/2 - PCB_RELIEF/2 + 0.01,0])
                    cube([PCB_WIDTH+2*PCB_GAP,PCB_RELIEF,PCB_HEIGHT+2*PCB_GAP],center=true);
                    // hole for optics path
                    rotate([90,0,0])
                    cylinder(h=2*COLUMN_DEPTH,d=OPTICS_SHROUD_OD,center=true);
                 
             }
             // standoffs for pcb mounts
             standoff_h = COLUMN_DEPTH - 2*PCB_THICKNESS;
             translate([PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([-PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([-PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             
             
             // arrow markers
//             translate([0,-COLUMN_DEPTH/2 + WALL_THICKNESS + COLUMN_MINK_R,COLUMN_HEIGHT/2])
//             rotate([0,0,90])
//             linear_extrude(0.5)
//             text("→",size=5,halign="center",valign="center");
//             translate([0,COLUMN_DEPTH/2 - WALL_THICKNESS - COLUMN_MINK_R,COLUMN_HEIGHT/2])
//             rotate([0,0,90])
//             linear_extrude(0.5)
//             text("→",size=5,halign="center",valign="center");
             
        }
       
        // socket for grip plug
        translate([0,0,+COLUMN_HEIGHT/2 - GRIP_SOCKET_HEIGHT/2 + 0.01])
        cube([GRIP_SOCKET_WIDTH,GRIP_SOCKET_DEPTH,GRIP_SOCKET_HEIGHT],center=true);  
        
        // holes for heat set inserts
        translate([PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([-PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([-PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
    }
}

// designed to light tight seal against the PCB, print with flexible material
module optics_shroud(){
    difference(){
        union(){
            // plug 
            cylinder(h=OPTICS_SHROUD_HEIGHT,d=OPTICS_SHROUD_OD);
            // base
            cylinder(h=OPTICS_SHROUD_BASE_HEIGHT,d=OPTICS_SHROUD_BASE_D);
        }
        // hollow
        translate([0,0,-0.01])
        cylinder(h=OPTICS_SHROUD_HEIGHT + 0.02,d=OPTICS_SHROUD_ID);
    }
}

// designed to capture cuvette, print with flexible material
module grip_plug_section(h,r2){
    difference(){
        //translate([0,0,-COLUMN_HEIGHT/2 + GRIP_SOCKET_HEIGHT/2 - 0.01])
        cube([GRIP_PLUG_WIDTH,GRIP_PLUG_DEPTH,h],center=true);  
        // create cavity for cuvette
        cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_DEPTH,CUVETTE_HEIGHT],center=true);
    }
    
    // place knurls all alongside inner edges
    dx = CUVETTE_CAVITY_WIDTH/(GRIP_INNER_KNURL_NUM+1);
    dy = CUVETTE_CAVITY_DEPTH/(GRIP_INNER_KNURL_NUM+1);
    x_nw = -CUVETTE_CAVITY_WIDTH/2;
    y_nw = CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        if (i == 1 || i == GRIP_INNER_KNURL_NUM){ //prevent smudging center of window
            x = x_nw + i*dx;
            translate([x,y_nw,0])
            cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
        }
    }
    x_ne = CUVETTE_CAVITY_WIDTH/2;
    y_ne = CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        y = y_ne - i*dy;
        translate([x_ne,y,0])
        cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
    }
    x_se = CUVETTE_CAVITY_WIDTH/2;
    y_se = -CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        if (i == 1 || i == GRIP_INNER_KNURL_NUM){ //prevent smudging center of window
            x = x_se - i*dx;
            translate([x,y_se,0])
            cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
        }
    }
    x_sw = -CUVETTE_CAVITY_WIDTH/2;
    y_sw = -CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        y = y_sw + i*dy;
        translate([x_sw,y,0])
        cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
    }
}


module grip_plug(){
    translate([0,0,GRIP_PLUG_HEIGHT/2 -0.01])
    grip_plug_section(h=GRIP_PLUG_HEIGHT/4 ,r2=0);
    grip_plug_section(h=3*GRIP_PLUG_HEIGHT/4 ,r2=GRIP_INNER_KNURL_R);
}

module base(){
    difference(){
        union(){
            translate([0,0,BASE_HEIGHT/2])
            cube([BASE_WIDTH, BASE_DEPTH,BASE_HEIGHT], center=true);
            translate([0,0,BASE_PLUG_HEIGHT/2])
            minkowski(){
                cube([BASE_PLUG_WIDTH-2*BASE_PLUG_MINK_R, 
                      BASE_PLUG_DEPTH-2*BASE_PLUG_MINK_R,
                      BASE_PLUG_HEIGHT-2*BASE_PLUG_MINK_R], center=true);
                sphere(BASE_PLUG_MINK_R);
            }
            translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
        }
        //mounting holes
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT -  0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        // screw head clearance holes
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        
        
    }
}

module assembly(){
    // column
    color(c=[0.5,0.5,0.5])
    translate([0,0,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    column();
    // base
    color("pink")
    base();
    // grip plug
    color("pink")
    translate([0,0,COLUMN_HEIGHT + BASE_HEIGHT - GRIP_PLUG_HEIGHT/2])
    grip_plug();
    // optics shrouds
    color("pink")
    translate([0,-COLUMN_DEPTH/2 + 1,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    rotate([-90,0,0])
    optics_shroud();
    color("pink")
    translate([0,+COLUMN_DEPTH/2 - 1,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    rotate([ 90,0,0])
    optics_shroud();
}


//column();

//optics_shroud();

//grip_plug();

//base();



//assembly();

//PCB_PCB = COLUMN_DEPTH-(PCB_STAND_OFF_DEPTH);
PCB_PCB = COLUMN_DEPTH - PCB_THICKNESS*2;
//PCB_PCB = 28;
translate([-COLUMN_WIDTH/2,-PCB_PCB/2,0])
color("blue",0.6)
//cube([COLUMN_WIDTH,PCB_PCB,COLUMN_HEIGHT]);

echo(PCB_PCB=PCB_PCB);

//PCB_PCB_OUTER = 28+PCB_THICKNESS;
PCB_PCB_OUTER = 20;

//PCB_WIDTH  = 34;
//PCB_HEIGHT = PCB_WIDTH; 
//PCB_STAND_OFF_DEPTH = 2;
//PCB_STAND_OFF_DIAM  = THREAD_INSERT_HOLE_DIAM + 4;
//PCB_THICKNESS = 1.75;
//PCB_RELIEF   = PCB_STAND_OFF_DEPTH + PCB_THICKNESS;
//PCB_GAP = 1;
//PCB_MOUNT_SPACING = 26;
//M3_THRU_HOLE_DIAM = 3.4;

/*
//emitter
difference(){
translate([-PCB_WIDTH/2,-PCB_PCB_OUTER/2,0])
color("green",0.6)
cube([PCB_WIDTH,PCB_THICKNESS,PCB_HEIGHT]);
    
//bottom left mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//top left mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);   

//top right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

}

//detector
difference(){
translate([-PCB_WIDTH/2,PCB_PCB_OUTER/2,0])
color("green",0.6)
cube([PCB_WIDTH,PCB_THICKNESS,PCB_HEIGHT]);

//bottom left mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_PCB_OUTER/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_PCB_OUTER/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//top left mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_PCB_OUTER/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);   

//top right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_PCB_OUTER/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);
}


PLATE_MOUNT_SPACING=42;
PLATE_BOUNDARY=PLATE_MOUNT_SPACING-PCB_MOUNT_SPACING+2*M3_THRU_HOLE_DIAM;

PLATE_WIDTH=PCB_WIDTH+PLATE_MOUNT_SPACING/2-PCB_MOUNT_SPACING/2+2*M3_THRU_HOLE_DIAM;
PLATE_HEIGHT=PCB_WIDTH;

SLIT_WIDTH=2;
SLIT_HEIGHT=12;
//emitter_side_plate

difference(){
translate([-PLATE_WIDTH/2,-PCB_PCB_OUTER/2+PCB_THICKNESS,0])
color("blue",0.6)
cube([PLATE_WIDTH,PCB_THICKNESS,PLATE_HEIGHT]);
    
//bottom left plate mount
translate([-PLATE_WIDTH/2+(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right plate mount
    translate([PLATE_WIDTH/2-(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//top left plate mount
    translate([-PLATE_WIDTH/2+(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);  

//top right plate mount
    translate([PLATE_WIDTH/2-(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);

//bottom left pcb mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS*2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*3,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS*2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*3,d=M3_THRU_HOLE_DIAM,center=true);

//top left pcb mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS*2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*3,d=M3_THRU_HOLE_DIAM,center=true);   

//top right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,-PCB_PCB_OUTER/2+PCB_THICKNESS*2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",0.6)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*3,d=M3_THRU_HOLE_DIAM,center=true);

//left slot
translate([-PLATE_WIDTH/2+(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2-SLIT_WIDTH/2,-PCB_PCB_OUTER/2,PLATE_HEIGHT/2-SLIT_HEIGHT/2])
color("blue",0.6)
cube([SLIT_WIDTH,PCB_THICKNESS*3,SLIT_HEIGHT]);

//right slot //**** 
translate([PLATE_WIDTH/2-(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2-SLIT_WIDTH/2,-PCB_PCB_OUTER/2,PLATE_HEIGHT/2-SLIT_HEIGHT/2])
color("blue",0.6)
cube([SLIT_WIDTH,PCB_THICKNESS*3,SLIT_HEIGHT]);

}

//pcb
*/
module pcb(){
   difference(){
   translate([-PCB_WIDTH/2,0,0]) 
    color("green",1)
cube([PCB_WIDTH,PCB_THICKNESS,PCB_HEIGHT]);
       
       //bottom left mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);
       
       //bottom right mount
translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);
   
       //top left mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);   

//top right mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("green",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1*2,d=M3_THRU_HOLE_DIAM,center=true);
}
       
}


PLATE_MOUNT_SPACING=PCB_WIDTH+2.5*M3_THRU_HOLE_DIAM;

SLIT_WIDTH=PCB_THICKNESS*1.1;
SLIT_HEIGHT=12;

PLATE_WIDTH=PLATE_MOUNT_SPACING+6*SLIT_WIDTH;
PLATE_HEIGHT=PCB_WIDTH+M3_THRU_HOLE_DIAM;
//emitter_side_plate

//STANDOFF_Z_INSET=2*M3_THRU_HOLE_DIAM;
STANDOFF_Z_INSET=(PCB_WIDTH-PCB_MOUNT_SPACING)/2+M3_THRU_HOLE_DIAM/2;
STANDOFF_X_OFFSET=PCB_WIDTH/2+M3_THRU_HOLE_DIAM;

LEFT_WALL_X_OFFSET=PCB_THICKNESS;

WALL_LENGTH=PCB_PCB_OUTER-PCB_THICKNESS*2;

WALL_TAB_WIDTH=6;
WALL_TAB_LENGTH=2;

module wall() {
translate([0,-WALL_LENGTH/2,0])
color("blue",1)
cube([PCB_THICKNESS,WALL_LENGTH,PLATE_HEIGHT]);

//tab
translate([0,-WALL_LENGTH/2-WALL_TAB_LENGTH,PLATE_HEIGHT/2-WALL_TAB_WIDTH])
color("blue",1)
cube([PCB_THICKNESS,WALL_TAB_LENGTH,WALL_TAB_WIDTH*2]);

//other tab

translate([0,WALL_LENGTH/2,PLATE_HEIGHT/2-WALL_TAB_WIDTH])
color("blue",1)
cube([PCB_THICKNESS,WALL_TAB_LENGTH,WALL_TAB_WIDTH*2]);
    
}

TAB_LENGTH=2;
TAB_WIDTH=4;

FLOOR_LEVEL=PCB_THICKNESS;
MID_HOLDER_LEVEL=STANDOFF_Z_INSET*1.5;
//MID_HOLDER_LEVEL=STANDOFF_Z_INSET+(STANDOFF_Z_INSET-PCB_THICKNESS);
//MID_HOLDER_LEVEL=STANDOFF_Z_INSET-FLOOR_LEVEL;

UPPER_HOLDER_LEVEL=PCB_HEIGHT;
NEAR_TOP_LEVEL=PCB_HEIGHT-STANDOFF_Z_INSET*1.2;

module floor() {
translate([-PLATE_WIDTH/2+TAB_LENGTH,-PCB_PCB_OUTER/2+PCB_THICKNESS,0])
color("purple",1)
cube([PLATE_WIDTH-2*TAB_LENGTH,PCB_PCB_OUTER-2*PCB_THICKNESS,PCB_THICKNESS]);

//tab
translate([-PLATE_WIDTH/2,-TAB_WIDTH/2,0])
color("purple",1)
cube([TAB_LENGTH,TAB_WIDTH,PCB_THICKNESS]);  

//other tab  
  translate([PLATE_WIDTH/2-TAB_LENGTH,-TAB_WIDTH/2,0])
color("purple",1)
cube([TAB_LENGTH,TAB_WIDTH,PCB_THICKNESS]);  
}


module holder() {
difference(){
floor();
    translate([-CUVETTE_CAVITY_WIDTH/2,-CUVETTE_CAVITY_WIDTH/2,-.1])
color("blue",0.6)
cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_WIDTH,PCB_THICKNESS*1.1]);
}
}




module plate(){
difference(){
translate([-PLATE_WIDTH/2,0,0])
color("purple",1)
cube([PLATE_WIDTH,PCB_THICKNESS,PLATE_HEIGHT]);
    
//bottom left plate mount
translate([-STANDOFF_X_OFFSET,PCB_THICKNESS/2,STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right plate mount
    translate([PLATE_WIDTH/2-(PLATE_WIDTH-PLATE_MOUNT_SPACING)/2,PCB_THICKNESS/2,STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

//top left plate mount
    translate([-STANDOFF_X_OFFSET,PCB_THICKNESS/2,PLATE_HEIGHT-STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);  

//top right plate mount
    translate([STANDOFF_X_OFFSET,PCB_THICKNESS/2,PLATE_HEIGHT-STANDOFF_Z_INSET])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

//bottom left pcb mount
translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

//bottom right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

//top left pcb mount
    translate([-PCB_WIDTH/2+(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);   

//top right pcb mount
    translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("purple",1)
rotate([90,0,0])
cylinder(h=PCB_THICKNESS*1.1,d=M3_THRU_HOLE_DIAM,center=true);

/*
//left slot
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,SLIT_WIDTH/2,PLATE_HEIGHT/2])
color("green",0.6)
//rotate([90,0,0])
cylinder(h=SLIT_HEIGHT,d=SLIT_WIDTH*1.1,center=true);

//right slot 
translate([PLATE_WIDTH/2-LEFT_WALL_X_OFFSET,SLIT_WIDTH/2,PLATE_HEIGHT/2])
color("green",0.6)
//rotate([90,0,0])
cylinder(h=SLIT_HEIGHT,d=SLIT_WIDTH*1.1,center=true);
*/

}
}


module standoff() {
        //translate([PCB_WIDTH/2-(PCB_WIDTH-PCB_MOUNT_SPACING)/2,PCB_THICKNESS/2,PCB_WIDTH-(PCB_WIDTH-PCB_MOUNT_SPACING)/2])
color("yellow",1)
rotate([90,0,0])
cylinder(h=PCB_PCB_OUTER+2*PCB_THICKNESS,d=M3_THRU_HOLE_DIAM,center=true);
}


// 'FLOORS'
translate([0,0,FLOOR_LEVEL])
floor();

translate([0,0,MID_HOLDER_LEVEL])
holder();

translate([0,0,NEAR_TOP_LEVEL])
holder();

translate([0,0,UPPER_HOLDER_LEVEL])
holder();


// 'CUVETTE'
translate([-CUVETTE_WIDTH/2,-CUVETTE_WIDTH/2,FLOOR_LEVEL+PCB_THICKNESS])
color("LightBlue",0.6)
cube([CUVETTE_WIDTH,CUVETTE_CAVITY_WIDTH,CUVETTE_HEIGHT]);



// 'WALLS'
translate([-PLATE_WIDTH/2+LEFT_WALL_X_OFFSET,0,0])
wall();

translate([PLATE_WIDTH/2-2*LEFT_WALL_X_OFFSET,0,0])
wall();




// 'STANDOFFS'
//bottom left standoff
translate([-STANDOFF_X_OFFSET,0,STANDOFF_Z_INSET])
standoff();

//bottom right standoff
    translate([STANDOFF_X_OFFSET,0,STANDOFF_Z_INSET])
standoff();

//top left standoff
    //translate([-STANDOFF_X_OFFSET,0,PCB_HEIGHT-STANDOFF_Z_INSET])
    translate([-STANDOFF_X_OFFSET,0,PLATE_HEIGHT-STANDOFF_Z_INSET])
standoff();  

//top right standoff
    translate([STANDOFF_X_OFFSET,0,PLATE_HEIGHT-STANDOFF_Z_INSET])
standoff();



// 'PLATES'
translate([0,PCB_PCB_OUTER/2-PCB_THICKNESS,0])
plate();

translate([0,-PCB_PCB_OUTER/2,0])
plate();


// 'PCBS'
translate([0,-PCB_PCB_OUTER/2-PCB_THICKNESS,0])
pcb();

translate([0,PCB_PCB_OUTER/2,0])
pcb();


/*
*/



