$fn = 100;
WALL_THICKNESS = 2.5;

// design for M3 brass heat set inserts
THREAD_INSERT_HOLE_DIAM = 4.75;
THREAD_INSERT_HOLE_LENGTH = 6;
THREAD_INSERT_HOLE_OFFSET = 30;


PCB_WIDTH  = 34;
PCB_HEIGHT = PCB_WIDTH; 
PCB_STAND_OFF_DEPTH = 2;
PCB_STAND_OFF_DIAM  = THREAD_INSERT_HOLE_DIAM + 4;
PCB_THICKNESS = 1.75;
PCB_RELIEF   = PCB_STAND_OFF_DEPTH + PCB_THICKNESS;
PCB_GAP = 1;
PCB_MOUNT_SPACING = 26;

OPTICS_SHROUD_WALL_T = 1;
OPTICS_SHROUD_ID = 8;
OPTICS_SHROUD_OD = OPTICS_SHROUD_ID + 2*OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_BASE_D = OPTICS_SHROUD_OD + OPTICS_SHROUD_WALL_T;
OPTICS_SHROUD_BASE_HEIGHT = 1.5*PCB_STAND_OFF_DEPTH;
OPTICS_SHROUD_HEIGHT = 3 + OPTICS_SHROUD_BASE_HEIGHT;



CUVETTE_HEIGHT = 45;
CUVETTE_WIDTH  = 12.6;
CUVETTE_DEPTH  = 12.4;
CUVETTE_GAP    = 0.5;
CUVETTE_HEIGHT_ABOVE_TOP = 10;

CUVETTE_CAVITY_WIDTH = CUVETTE_WIDTH+2*CUVETTE_GAP;
CUVETTE_CAVITY_DEPTH = CUVETTE_DEPTH+2*CUVETTE_GAP;


COLUMN_WIDTH = PCB_WIDTH + 4*WALL_THICKNESS;
COLUMN_DEPTH = CUVETTE_DEPTH + 6*WALL_THICKNESS;
COLUMN_HEIGHT = PCB_HEIGHT + 2*PCB_RELIEF + 4*WALL_THICKNESS;

COLUMN_MINK_R = 2;



// plug of flexible material for holding cuvette
GRIP_PLUG_WIDTH  = CUVETTE_CAVITY_WIDTH + 2*WALL_THICKNESS;
GRIP_PLUG_DEPTH  = CUVETTE_CAVITY_DEPTH + 2*WALL_THICKNESS;
GRIP_PLUG_HEIGHT = CUVETTE_HEIGHT_ABOVE_TOP + 2*WALL_THICKNESS;
GRIP_PLUG_GAP = 0.1;
GRIP_INNER_KNURL_R = 1.1*CUVETTE_GAP;//
GRIP_INNER_KNURL_NUM = 4;

// socket in top of column to fit grip plug
GRIP_SOCKET_WIDTH = GRIP_PLUG_WIDTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_DEPTH = GRIP_PLUG_DEPTH + 2*GRIP_PLUG_GAP;
GRIP_SOCKET_HEIGHT = GRIP_PLUG_HEIGHT;

// base plug, fits into bottom, stops cuvete moving down, dampens vibration
// should be printed in flexible, soft, material
BASE_WIDTH = COLUMN_WIDTH + 4*WALL_THICKNESS;
BASE_DEPTH = COLUMN_DEPTH + 4*WALL_THICKNESS;
BASE_HEIGHT = 2*WALL_THICKNESS;
BASE_FOOT_DIAM = 8;
BASE_FOOT_HEIGHT = WALL_THICKNESS;


BASE_PLUG_WIDTH = CUVETTE_CAVITY_WIDTH;
BASE_PLUG_DEPTH = CUVETTE_CAVITY_DEPTH;
BASE_PLUG_HEIGHT = COLUMN_HEIGHT - (CUVETTE_HEIGHT - CUVETTE_HEIGHT_ABOVE_TOP) + BASE_HEIGHT;
BASE_PLUG_MINK_R = 2;

M3_THRU_HOLE_DIAM = 3.4;
M3_HEAD_CLEAR_DIAM = 6;

// should be printed with rigid material, preferably matte black
module column(){
    difference(){
        union(){
             difference(){
                 minkowski(){
                    cube([COLUMN_WIDTH-2*COLUMN_MINK_R,COLUMN_DEPTH-2*COLUMN_MINK_R,COLUMN_HEIGHT-2*COLUMN_MINK_R],center=true);
                    sphere(COLUMN_MINK_R);
                 }
                    // create cavity for cuvette
                    cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_DEPTH,2*CUVETTE_HEIGHT],center=true);
                    // make relief for source board
                    translate([0,-COLUMN_DEPTH/2 + PCB_RELIEF/2 - 0.01,0])
                    cube([PCB_WIDTH+2*PCB_GAP,PCB_RELIEF,PCB_HEIGHT+2*PCB_GAP],center=true);
                    // make relief for source board
                    translate([0, COLUMN_DEPTH/2 - PCB_RELIEF/2 + 0.01,0])
                    cube([PCB_WIDTH+2*PCB_GAP,PCB_RELIEF,PCB_HEIGHT+2*PCB_GAP],center=true);
                    // hole for optics path
                    rotate([90,0,0])
                    cylinder(h=2*COLUMN_DEPTH,d=OPTICS_SHROUD_OD,center=true);
                 
             }
             // standoffs for pcb mounts
             standoff_h = COLUMN_DEPTH - 2*PCB_THICKNESS;
             translate([PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([-PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             translate([-PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
             rotate([90,0,0])
             cylinder(h=standoff_h,d=PCB_STAND_OFF_DIAM,center=true);
             
             
             // arrow markers
//             translate([0,-COLUMN_DEPTH/2 + WALL_THICKNESS + COLUMN_MINK_R,COLUMN_HEIGHT/2])
//             rotate([0,0,90])
//             linear_extrude(0.5)
//             text("→",size=5,halign="center",valign="center");
//             translate([0,COLUMN_DEPTH/2 - WALL_THICKNESS - COLUMN_MINK_R,COLUMN_HEIGHT/2])
//             rotate([0,0,90])
//             linear_extrude(0.5)
//             text("→",size=5,halign="center",valign="center");
             
        }
       
        // socket for grip plug
        translate([0,0,+COLUMN_HEIGHT/2 - GRIP_SOCKET_HEIGHT/2 + 0.01])
        cube([GRIP_SOCKET_WIDTH,GRIP_SOCKET_DEPTH,GRIP_SOCKET_HEIGHT],center=true);  
        
        // holes for heat set inserts
        translate([PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([-PCB_MOUNT_SPACING/2,0,PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
        translate([-PCB_MOUNT_SPACING/2,0,-PCB_MOUNT_SPACING/2])
        rotate([90,0,0])
        cylinder(h=2*COLUMN_DEPTH,d=THREAD_INSERT_HOLE_DIAM,center=true);
    }
}

// designed to light tight seal against the PCB, print with flexible material
module optics_shroud(){
    difference(){
        union(){
            // plug 
            cylinder(h=OPTICS_SHROUD_HEIGHT,d=OPTICS_SHROUD_OD);
            // base
            cylinder(h=OPTICS_SHROUD_BASE_HEIGHT,d=OPTICS_SHROUD_BASE_D);
        }
        // hollow
        translate([0,0,-0.01])
        cylinder(h=OPTICS_SHROUD_HEIGHT + 0.02,d=OPTICS_SHROUD_ID);
    }
}

// designed to capture cuvette, print with flexible material
module grip_plug_section(h,r2){
    difference(){
        //translate([0,0,-COLUMN_HEIGHT/2 + GRIP_SOCKET_HEIGHT/2 - 0.01])
        cube([GRIP_PLUG_WIDTH,GRIP_PLUG_DEPTH,h],center=true);  
        // create cavity for cuvette
        cube([CUVETTE_CAVITY_WIDTH,CUVETTE_CAVITY_DEPTH,CUVETTE_HEIGHT],center=true);
    }
    
    // place knurls all alongside inner edges
    dx = CUVETTE_CAVITY_WIDTH/(GRIP_INNER_KNURL_NUM+1);
    dy = CUVETTE_CAVITY_DEPTH/(GRIP_INNER_KNURL_NUM+1);
    x_nw = -CUVETTE_CAVITY_WIDTH/2;
    y_nw = CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        if (i == 1 || i == GRIP_INNER_KNURL_NUM){ //prevent smudging center of window
            x = x_nw + i*dx;
            translate([x,y_nw,0])
            cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
        }
    }
    x_ne = CUVETTE_CAVITY_WIDTH/2;
    y_ne = CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        y = y_ne - i*dy;
        translate([x_ne,y,0])
        cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
    }
    x_se = CUVETTE_CAVITY_WIDTH/2;
    y_se = -CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        if (i == 1 || i == GRIP_INNER_KNURL_NUM){ //prevent smudging center of window
            x = x_se - i*dx;
            translate([x,y_se,0])
            cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
        }
    }
    x_sw = -CUVETTE_CAVITY_WIDTH/2;
    y_sw = -CUVETTE_CAVITY_DEPTH/2;
    for (i = [1:GRIP_INNER_KNURL_NUM]){
        y = y_sw + i*dy;
        translate([x_sw,y,0])
        cylinder(h=h,r1=GRIP_INNER_KNURL_R,r2=r2, center=true);
    }
}


module grip_plug(){
    translate([0,0,GRIP_PLUG_HEIGHT/2 -0.01])
    grip_plug_section(h=GRIP_PLUG_HEIGHT/4 ,r2=0);
    grip_plug_section(h=3*GRIP_PLUG_HEIGHT/4 ,r2=GRIP_INNER_KNURL_R);
}

module base(){
    difference(){
        union(){
            translate([0,0,BASE_HEIGHT/2])
            cube([BASE_WIDTH, BASE_DEPTH,BASE_HEIGHT], center=true);
            translate([0,0,BASE_PLUG_HEIGHT/2])
            minkowski(){
                cube([BASE_PLUG_WIDTH-2*BASE_PLUG_MINK_R, 
                      BASE_PLUG_DEPTH-2*BASE_PLUG_MINK_R,
                      BASE_PLUG_HEIGHT-2*BASE_PLUG_MINK_R], center=true);
                sphere(BASE_PLUG_MINK_R);
            }
            translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
            translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT + 0.01])
            cylinder(h=BASE_FOOT_HEIGHT,d=BASE_FOOT_DIAM);
        }
        //mounting holes
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT - 0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,-BASE_FOOT_HEIGHT -  0.01])
        cylinder(h=2*BASE_HEIGHT,d=M3_THRU_HOLE_DIAM);
        // screw head clearance holes
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,-BASE_DEPTH/2+BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([-BASE_WIDTH/2+BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        translate([ BASE_WIDTH/2-BASE_FOOT_DIAM/2,BASE_DEPTH/2-BASE_FOOT_DIAM/2,BASE_HEIGHT/2])
        cylinder(h=2*BASE_HEIGHT,d=M3_HEAD_CLEAR_DIAM);
        
        
    }
}

module assembly(){
    // column
    color(c=[0.5,0.5,0.5])
    translate([0,0,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    column();
    // base
    //color("pink")
    //base();
    // grip plug
    //color("pink")
    //translate([0,0,COLUMN_HEIGHT + BASE_HEIGHT - GRIP_PLUG_HEIGHT/2])
    //  grip_plug();
    // optics shrouds
    color("pink")
    translate([0,-COLUMN_DEPTH/2 + 1,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    rotate([-90,0,0])
    optics_shroud();
    color("pink")
    translate([0,+COLUMN_DEPTH/2 - 1,COLUMN_HEIGHT/2 + BASE_HEIGHT])
    rotate([ 90,0,0])
    optics_shroud();
}


//column();

//optics_shroud();

//grip_plug();

//base();


assembly();

//PCB_PCB = COLUMN_DEPTH-(PCB_STAND_OFF_DEPTH);
PCB_PCB = COLUMN_DEPTH - PCB_THICKNESS*2;
translate([-COLUMN_WIDTH/2,-PCB_PCB/2,0])
color("blue",0.6)
cube([COLUMN_WIDTH,PCB_PCB,COLUMN_HEIGHT]);

echo(PCB_PCB=PCB_PCB);

module emitter(){
    // pcb
    PCB_CENTER_HEIGHT=(COLUMN_HEIGHT/2 + BASE_HEIGHT)-PCB_HEIGHT/2;
    PCB_CENTER_X=-PCB_WIDTH/2;
    color("green",0.6)
    translate([PCB_CENTER_X,-(PCB_PCB/2+PCB_THICKNESS),(COLUMN_HEIGHT/2 + BASE_HEIGHT)-PCB_HEIGHT/2])
    cube([PCB_WIDTH,PCB_THICKNESS,PCB_HEIGHT]);
    // right-angle
    
}


emitter();

module connector(){
    // pcb
    CONNECTOR_WIDTH=2.54*6;
    CONNECTOR_HEIGHT=8.5+2.5; //right-angle + female socket
    PCB_CENTER_HEIGHT=(COLUMN_HEIGHT/2 + BASE_HEIGHT)-PCB_HEIGHT/2;
    PCB_CENTER_X=-PCB_WIDTH/2;
    color("black",0.6)
    translate([-CONNECTOR_WIDTH/2,-(PCB_PCB/2+PCB_THICKNESS)-2.5,(COLUMN_HEIGHT/2 + BASE_HEIGHT)-PCB_HEIGHT/2-CONNECTOR_HEIGHT])
    cube([CONNECTOR_WIDTH,2.5,CONNECTOR_HEIGHT]);
    // right-angle
    
}

connector();

OUTER_PCB_DISTANCE=PCB_PCB+2*PCB_THICKNESS;
echo(OUTER_PCB_DISTANCE=OUTER_PCB_DISTANCE);

//MOTHER_CONNECTORS_CENTER_DISTANCE=OUTER_PCB_DISTANCE+2*(2.5/2);
MOTHER_CONNECTORS_CENTER_DISTANCE=30+2*(2.5/2); //(rounding up the distance between the PCBs)

echo(MOTHER_CONNECTORS_CENTER_DISTANCE=MOTHER_CONNECTORS_CENTER_DISTANCE);

NEW_PCB_PCB=24;
NEW_PCB_THICKNESS=1.7;
NEW_OUTER_PCB_DISTANCE=NEW_PCB_PCB+2*NEW_PCB_THICKNESS;
NEW_MOTHER_CONNECTORS_CENTER_DISTANCE=NEW_OUTER_PCB_DISTANCE+2*(2.5/2);
echo(NEW_MOTHER_CONNECTORS_CENTER_DISTANCE=NEW_MOTHER_CONNECTORS_CENTER_DISTANCE);

multiple=NEW_MOTHER_CONNECTORS_CENTER_DISTANCE/2.54;
echo(multiple=multiple);
echo(COLUMN_WIDTH=COLUMN_WIDTH);