# UV Water

More explanations and notes at https://edgecollective.io/notes/uvf/

For specific board versions, check out the [Releases](https://gitlab.com/edgecollective/uv-water/-/releases).

Also check out the GitLab Pages site for this project: https://edgecollective.gitlab.io/uv-water/